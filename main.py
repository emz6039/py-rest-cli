import requests
import os
import json
from dotenv import load_dotenv
import argparse

load_dotenv()

BASE_URL = os.getenv('BASE_URL')

parser = argparse.ArgumentParser(description="Run REST calls cli")
parser.add_argument('url', nargs=1, help='Relative url path')
parser.add_argument('type', nargs=1, help='[ POST | GET | PUT | POST | DELETE]')
parser.add_argument('--payload', help='file used for request payload, relative path to where script is ran')
parser.add_argument('--headers', help='file used for request headers, relative path to where script is ran')
parser.add_argument('--output', help='file to dump http response')


def get_base_path():
    cwd = os.getcwd()
    return cwd


def build_path(*args):
    return os.path.join(*args)


def output_file(output_path, output):
    with open(output_path, 'w+') as file:
        file.write(output)


def get_payload(filename=None):
    if filename:
        payload_path = build_path(get_base_path(), filename)
    else:
        payload_path = build_path(get_base_path())
    with open(payload_path, "r") as file:
        payload = json.loads(file.read())
    return payload


def get_headers(custom_headers):
    headers = {}
    if custom_headers:
        headers_path = build_path(get_base_path(), custom_headers)
    else:
        headers_path = build_path(get_base_path(), "headers.json")
    with open(headers_path, "r") as file:
        headers = json.loads(file.read())
    return headers


def handle_request(type, url, payload_path, output_path, header_path):
    data = get_payload(payload_path)
    res = requests.request(type, f'{BASE_URL}/{url}', data=json.dumps(data), headers=get_headers(header_path))
    text = res.text
    print(f"Returning result! {res.status_code} {res.text}")
    if output_path:
        output_file(output_path, text)
    else:
        print(text)


def build_request(**kwargs):
    kwargs.update({"payload_path": "payload.json", "output_path": None, "header_path": None})
    if args.payload:
        kwargs['payload_path'] = args.payload
    if args.output:
        kwargs['output_path'] = args.output
    if args.headers:
        kwargs['header_path'] = args.headers
    handle_request(**kwargs)
    # output path payload path


def handle_args(args):
    if args.url and args.type:
        build_request(url=args.url[0], type=args.type[0].upper())


if __name__ == "__main__":
    args = parser.parse_args()
    handle_args(args)
